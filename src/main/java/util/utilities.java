package util;

import io.opentelemetry.sdk.metrics.data.Data;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class utilities {
    static String randomNumber;
    public static String getTimestamp(){
        Date d1 = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("DD-MM-YYYY:HH:MM:SS");
        System.out.println(sdf.format(d1));
        randomNumber=sdf.format(d1).toLowerCase().replace(":","-");
        return randomNumber;
    }

    public static String captureScreenshot(WebDriver driver, String testName) {

        File srcScreenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        String destinationScreenshotPath = System.getProperty("user.dir")+"/Screenshots/"+testName+".png";

        try {
            FileHandler.copy(srcScreenshot,new File(destinationScreenshotPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return destinationScreenshotPath;
    }



}


