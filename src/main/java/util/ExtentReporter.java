package util;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ExtentReporter {
    public static ExtentReports generateExtentReport() throws IOException {
        ExtentReports extentReport = new ExtentReports();
        File extentReportFile = new File(System.getProperty("user.dir")+"/src/test-output/extentreports.html");
        ExtentSparkReporter sparkReporter = new ExtentSparkReporter(extentReportFile);
        sparkReporter.config().setTheme(Theme.DARK);
        sparkReporter.config().setReportName("Automation Ninja Test Automation Report");
        sparkReporter.config().setDocumentTitle("TN Automation Reports");
        sparkReporter.config().setTimeStampFormat("DD-MM-YYYY hh:mm:ss");
        extentReport.attachReporter(sparkReporter);
        Properties configProp = new Properties();
        File file =new File(System.getProperty("user.dir")+"/src/main/java/testdata/testdata.properties");
        FileInputStream fis = new FileInputStream(file);
        configProp.load(fis);
        extentReport.setSystemInfo("Application URL",configProp.getProperty("url"));
        extentReport.setSystemInfo("BrowserName",configProp.getProperty("browsername"));
        extentReport.setSystemInfo("UserName",configProp.getProperty("validusername"));
        extentReport.setSystemInfo("Password",configProp.getProperty("validpassword"));
        extentReport.setSystemInfo("Operating System", System.getProperty("os.name"));
        extentReport.setSystemInfo("Username",System.getProperty("user.name"));
        extentReport.setSystemInfo("Java Version", System.getProperty("java.version"));
        return extentReport;
    }
}
