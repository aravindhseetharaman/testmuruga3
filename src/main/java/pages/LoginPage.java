package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.devtools.v85.page.Page;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    WebDriver driver;
    MyAccountPage myAccountPage;

    public LoginPage(WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    private String userName;
    private String passWord;
    @FindBy(xpath = "//input[@id='input-email']")
    WebElement emailIdTxtBox;

    @FindBy(xpath ="//input[@id='input-password']")
    WebElement passwordTxtBox;

    @FindBy(xpath = "//input[@value='Login']")
    WebElement loginButton;


    public void enter_username_password(String username,String password){
        emailIdTxtBox.sendKeys(username);
        passwordTxtBox.sendKeys(password);
    }


    public MyAccountPage accountPage(){
        loginButton.click();
        myAccountPage=new MyAccountPage(driver);
        return myAccountPage;

    }
}
