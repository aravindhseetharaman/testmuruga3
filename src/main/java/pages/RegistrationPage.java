package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPage {
   WebDriver driver;
   RegistrationSuccessPage registrationSuccessPage;

   public RegistrationPage(WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    Boolean duplicateAlertMessage;

    @FindBy(xpath ="//input[@id='input-firstname']")
    private WebElement firstnameTxtBox;

    @FindBy(xpath = "//input[@id='input-lastname']")
    private WebElement lastnameTxtBox;

    @FindBy(xpath="//input[@id='input-email']")
    private WebElement emailTxtBox;

    @FindBy(id = "input-telephone")
    private WebElement telephoneTxtBox;

    @FindBy(xpath="//input[@id='input-password']")
    private WebElement passwordTxtBox;

    @FindBy(xpath = "//input[@id='input-confirm']")
    private WebElement passwordConfirmTxtBox;

    @FindBy(xpath = "//input[@name='agree']")
    private WebElement privacyPolicyCheckBox;

    @FindBy(xpath = "//input[@value='Continue']")
    private WebElement continueButton;

    @FindBy(xpath="//div[@class='alert alert-danger alert-dismissible']")
    private WebElement DuplicateCustomerAlert;

    @FindBy(xpath="//div[contains(@class,'alert-dismissible')]")
    private WebElement duplicateEmailAddressWarning;

    @FindBy(xpath="//div[contains(@class,'alert-dismissible')]")
    private WebElement privacyPolicyWarning;

    @FindBy(xpath="//input[@id='input-firstname']/following-sibling::div")
    private WebElement firstNameWarning;

    @FindBy(xpath="//input[@id='input-lastname']/following-sibling::div")
    private WebElement lastNameWarning;

    @FindBy(xpath="//input[@id='input-email']/following-sibling::div")
    private WebElement emailWarning;

    @FindBy(xpath="//input[@id='input-telephone']/following-sibling::div")
    private WebElement telephoneWarning;

    @FindBy(xpath="//input[@id='input-password']/following-sibling::div")
    private WebElement passwordWarning;



    public void customerDataEntry(String firstName,String lastName,String email,String telephone,String password,String confirmpassword) {
        firstnameTxtBox.sendKeys(firstName);
        lastnameTxtBox.sendKeys(lastName);
        emailTxtBox.sendKeys(email);
        telephoneTxtBox.sendKeys(telephone);
        passwordTxtBox.sendKeys(password);
        passwordConfirmTxtBox.sendKeys(confirmpassword);
        privacyPolicyCheckBox.click();
    }

    public boolean getDuplicateCustomerAlert() {
        continueButton.click();
        boolean alertGeneratedYesorNo;
        alertGeneratedYesorNo= DuplicateCustomerAlert.isDisplayed();
        return alertGeneratedYesorNo;
    }

    public String getFirstNameWarning() {
        continueButton.click();
        return firstNameWarning.getText();
    }

    public String getLastNameWarning() {
        continueButton.click();
        return lastNameWarning.getText();
    }

    public String getEmailWarning() {
        continueButton.click();
        return emailWarning.getText();
    }

    public String getTelephoneWarning() {
        continueButton.click();
        return telephoneWarning.getText();
    }

    public String getPasswordWarning() {
        continueButton.click();
        return passwordWarning.getText();
    }





    public RegistrationSuccessPage RegistrationSuccesful(){
        continueButton.click();
        registrationSuccessPage = new RegistrationSuccessPage(driver);
        return registrationSuccessPage;
    }





}


