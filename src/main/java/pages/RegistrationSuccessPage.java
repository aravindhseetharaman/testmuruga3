package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegistrationSuccessPage {
    WebDriver driver;
    SearchPage searchPage;


    public RegistrationSuccessPage(WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy (xpath = "//a[@class='btn btn-primary']")
    WebElement successMessageContinueBtn;


    public SearchPage click_successcontineu(){
        successMessageContinueBtn.click();
        searchPage=new SearchPage(driver);
        return searchPage;
    }
}
