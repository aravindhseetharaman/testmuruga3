package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HomePage {
    LoginPage loginPage;
    RegistrationPage registrationPage;
    WebDriver driver;
    //ul[@class='dropdown-menu dropdown-menu-right']//a[normalize-space()='Login']
    @FindBy( xpath = "//span[normalize-space()='My Account']")
    WebElement myAccountMenu;

    @FindBy (xpath = "//a[normalize-space()='Login']")
    WebElement myLoginMenu;

    @FindBy(linkText = "Register")
    //ul[@class='dropdown-menu dropdown-menu-right']//a[normalize-space()='Register']
     WebElement registerMenu;

    public HomePage(WebDriver driver) {
       this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    public void myAccountMenuClick(){
        myAccountMenu.click();
    }

    public LoginPage loginMenuClick(){
         myLoginMenu.click();
         loginPage =new LoginPage(driver);
         return loginPage;
    }

    public RegistrationPage registerMenuClick(){
        registerMenu.click();
        registrationPage =new RegistrationPage(driver);
        return registrationPage;
    }
}
