package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPage {
    WebDriver driver;
    
    @FindBy(xpath = "//a[normalize-space()='Edit your account information']")
    WebElement edityouraccountLbl;
    boolean edityouraccMsg;
    
    public boolean verify_edityouraccounmessage_displayed(){
        edityouraccMsg= edityouraccountLbl.isDisplayed();
        return edityouraccMsg;
    }

    public SearchPage(WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy (xpath = "//input[@placeholder='Search']")
    WebElement searchTxtBox;

    @FindBy (xpath ="//button[@class='btn btn-default btn-lg']")
    private WebElement searchIcon;

    @FindBy (xpath = "//i[@class='fa fa-search']")
    private WebElement searchIcon2;



    @FindBy (linkText = "HP LP3065")
    private WebElement validSearchResult;

    @FindBy (xpath = "//p[contains(text(),'There is no product that matches the search criter')]")
    private WebElement invalidSearchResult;



    public String validsearch(String searchItem){
        searchTxtBox.sendKeys(searchItem);
        searchIcon.click();
        return validSearchResult.getText();
    }

    public String Invalidsearch(String searchItem){
        searchTxtBox.sendKeys(searchItem);
        searchIcon2.click();
        return invalidSearchResult.getText();
    }


}
