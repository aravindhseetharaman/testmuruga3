package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage {
    WebDriver driver;
    boolean loginSuccesfulYorN;
    boolean warningMessageYorN;
    public MyAccountPage(WebDriver driver) {
        this.driver=driver;
        PageFactory.initElements(this.driver,this);
    }

    @FindBy(xpath = "//a[normalize-space()='Edit your account information']" )
    WebElement edityouraccountinfoMenu;

    @FindBy(xpath = "//div[@class='alert alert-danger alert-dismissible']")
    WebElement warningMessageLoginNotsuccesful;

    public boolean accountLoginSuccesful(){
        loginSuccesfulYorN = edityouraccountinfoMenu.isDisplayed();
        return loginSuccesfulYorN;
    }

    public boolean accountLoginFailure(){
        warningMessageYorN = warningMessageLoginNotsuccesful.isDisplayed();
        return warningMessageYorN;
    }
}
