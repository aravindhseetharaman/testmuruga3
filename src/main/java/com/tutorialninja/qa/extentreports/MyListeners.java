package com.tutorialninja.qa.extentreports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import util.ExtentReporter;
import util.utilities;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;

import static util.utilities.captureScreenshot;

public class MyListeners implements ITestListener {
    ExtentReports extentReports;
    ExtentTest extentTest;
    String testName;
    @Override
    public void onTestStart(ITestResult result) {
        testName=result.getName();
        System.out.println(result.getName()+"execution of project tests started.....");
         extentTest = extentReports.createTest(testName);
         extentTest.log(Status.INFO,testName +"Started Executing..");

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        testName=result.getName();
        System.out.println(testName+"execution completed succesfully..");

    }

    @Override
    public void onTestFailure(ITestResult result) {
        WebDriver driver = null;
        testName=result.getName();
        System.out.println(testName+"execution failed..");
        System.out.println("Screenshot Taken...");

        try {
            driver = (WebDriver)result.getTestClass().getRealClass().getDeclaredField("driver").get(result.getInstance());
        } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }

        String destinationScreenshotPath = utilities.captureScreenshot(driver,result.getName());

        extentTest.addScreenCaptureFromPath(destinationScreenshotPath);
        extentTest.log(Status.INFO,result.getThrowable());
        extentTest.log(Status.FAIL,result.getName()+" got failed");

    }

    @Override
    public void onTestSkipped(ITestResult result) {
        testName =result.getName();
        extentTest.log(Status.INFO,result.getThrowable());
        extentTest.log(Status.SKIP,testName +"got Skipped");

    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println("Test execution started.....");

        try {
            extentReports = ExtentReporter.generateExtentReport();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onFinish(ITestContext context) {
        System.out.println(context.getName()+"execution finidhed..");
        extentReports.flush();
        String pathOfExtentReport = System.getProperty("user.dir")+"/src/test-output/extentreports/extentreports.html";
        File extentReport = new File(pathOfExtentReport);

        try {
            Desktop.getDesktop().browse(extentReport.toURI());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
