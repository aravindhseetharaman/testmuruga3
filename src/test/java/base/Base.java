package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

public class Base {
    String browserType;
    public WebDriver driver;
    public Properties prop_config,prop_testdata;
    WebDriverWait wait;
public WebDriver initializeBrowser(){
    browserType = prop_config.getProperty("browserName");
    switch(browserType) {
        case "chrome":
            driver = new ChromeDriver();
            break;
        case "edge":
            driver = new EdgeDriver();
            break;
        case "firefox":
            driver = new FirefoxDriver();
            break;
        default:
            // code block
    }


    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
    driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(25));
    driver.get(prop_config.getProperty("url"));
    return driver;
}


    public Base() throws IOException {
        try {
            File file_config = new File(System.getProperty("user.dir")+"/src/main/java/config/configure.properties");
            File file_testdata = new File(System.getProperty("user.dir")+"/src/main/java/testdata/testdata.properties");
            FileInputStream fis_config = new FileInputStream(file_config);
            FileInputStream fis_testdata = new FileInputStream(file_testdata);
            prop_config = new Properties();
            prop_testdata = new Properties();
            prop_config.load(fis_config);
            prop_testdata.load(fis_testdata);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
