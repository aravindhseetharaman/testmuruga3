package tutorialninja.qa.testcases;

import base.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.MyAccountPage;
import pages.SearchPage;


import java.io.BufferedInputStream;
import java.io.IOException;

public class SearchTest extends Base {

    HomePage homePage;
    LoginPage loginPage;
    MyAccountPage myAccountPage;
    SearchPage searchPage;
    public WebDriver driver;
    LoginTest loginTest;
    WebElement searchTxtBox;
    WebElement searchIcon;
    WebElement validsearchResult;
    WebElement inValidSearchResult;
    String searchResult;
    WebElement myAccountMenu;
    WebElement myLoginMenu;
    WebElement emailIdTxtBox;
    WebElement passwordTxtBox;
    WebElement loginButton;


    public SearchTest() throws IOException {
        super();
    }

    @BeforeMethod
    public void setup() throws IOException {
        driver = initializeBrowser();
        homePage =new HomePage(driver);
    }

    public void login_dataentry(){
        homePage.myAccountMenuClick();
        loginPage = homePage.loginMenuClick();
    }

    @Test(priority = 9)
    public void validSearch() throws IOException {
        login_dataentry();
        loginPage.enter_username_password(prop_testdata.getProperty("validusername"),prop_testdata.getProperty("validpassword"));
        myAccountPage=loginPage.accountPage();
        searchPage=new SearchPage(driver);
        searchResult= searchPage.validsearch(prop_testdata.getProperty("validproduct"));
        Assert.assertEquals(searchResult,prop_testdata.getProperty("validsearchresult"),"Valid Search works..");

  }

    @Test(priority = 10)
    public void invalidSearch() throws IOException {
        homePage=new HomePage(driver);
        homePage.myAccountMenuClick();
        loginPage = homePage.loginMenuClick();
        loginPage.enter_username_password(prop_testdata.getProperty("validusername"),prop_testdata.getProperty("validpassword"));
        myAccountPage = loginPage.accountPage();
        searchPage = new SearchPage(driver);
        searchResult = searchPage.Invalidsearch(prop_testdata.getProperty("invalidproduct"));
        Assert.assertEquals(searchResult, prop_testdata.getProperty("NoProductTextInSearchResults"),"InValid search ..noluck..");

    }



    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
