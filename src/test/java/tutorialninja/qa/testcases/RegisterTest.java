package tutorialninja.qa.testcases;

import base.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.*;

import javax.swing.text.Utilities;
import java.io.IOException;

public class RegisterTest extends Base {

    public WebDriver driver;
    HomePage homePage;
    RegistrationPage registrationPage;
    SearchPage searchPage;
    RegistrationSuccessPage registrationSuccessPage;


    @BeforeMethod
    public void setup(){
        driver= initializeBrowser();
        homePage=new HomePage(driver);
    }

    public void dataentry_customerreg(){
        homePage.myAccountMenuClick();
        registrationPage=homePage.registerMenuClick();
    }

    @Test(priority = 6)
    public void customerReg_HappyPath(){
        dataentry_customerreg();
        registrationPage.customerDataEntry(prop_testdata.getProperty("firstname"),prop_testdata.getProperty("lastname"),prop_testdata.getProperty("email")+ util.utilities.getTimestamp()+"@gmail.com",prop_testdata.getProperty("telephone"),prop_testdata.getProperty("password"),prop_testdata.getProperty("passwordconfirm"));
        registrationSuccessPage=registrationPage.RegistrationSuccesful();
        searchPage=registrationSuccessPage.click_successcontineu();
        Assert.assertTrue(searchPage.verify_edityouraccounmessage_displayed(),"Edit your account info displayed");
    }

    @Test(priority = 7)
    public void customerReg_duplicateEmail(){
        dataentry_customerreg();
        registrationPage.customerDataEntry(prop_testdata.getProperty("firstname"),prop_testdata.getProperty("lastname"),prop_testdata.getProperty("duplicateemail"),prop_testdata.getProperty("telephone"),prop_testdata.getProperty("password"),prop_testdata.getProperty("passwordconfirm"));
        Assert.assertTrue(registrationPage.getDuplicateCustomerAlert(),"Warning for Duplicate alert generated....");
    }

    @Test(priority = 8)
    public void customerReg_AllBlanks(){
        dataentry_customerreg();
        registrationPage.customerDataEntry("","","","","","");
        Assert.assertEquals(registrationPage.getFirstNameWarning(),prop_testdata.getProperty("firstnamewarning"),"Validation of Firstname Warning!!");
        Assert.assertEquals(registrationPage.getLastNameWarning(),prop_testdata.getProperty("lastnamewarning"),"Validation of LastName Warning!!");
        Assert.assertEquals(registrationPage.getPasswordWarning(),prop_testdata.getProperty("passwordwarning"),"Validation of Passsword Warning!!");
        Assert.assertEquals(registrationPage.getEmailWarning(),prop_testdata.getProperty("emailwarning"),"Validation of Email Warning!!");
        Assert.assertEquals(registrationPage.getTelephoneWarning(),prop_testdata.getProperty("telephonewarning"),"Validation of Telephone Warning!!");
    }



    public RegisterTest() throws IOException {
        super();
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
