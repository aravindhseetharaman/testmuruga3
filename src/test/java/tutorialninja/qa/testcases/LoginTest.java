package tutorialninja.qa.testcases;

import base.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.MyAccountPage;


import java.io.IOException;


public class LoginTest extends Base {
    public WebDriver driver;


    HomePage homePage;
    LoginPage loginPage;
    MyAccountPage myAccountPage;
    public LoginTest() throws IOException {
        super();
    }

    @BeforeMethod
    public void setup(){
        driver = initializeBrowser();
        homePage =new HomePage(driver);

    }

    public void login_dataentry(){
        homePage.myAccountMenuClick();
        loginPage = homePage.loginMenuClick();
    }
    @Test(priority = 1)
    public void login_validuser_validpassword() {
        login_dataentry();
        loginPage.enter_username_password(prop_testdata.getProperty("validusername"),prop_testdata.getProperty("validpassword"));
        myAccountPage=loginPage.accountPage();
        Assert.assertTrue( myAccountPage.accountLoginSuccesful(),"Login with valid user/valid password is succesful");
    }

    @Test(priority = 2)
    public void login_validuser_invalidpassword(){
        login_dataentry();
        loginPage.enter_username_password(prop_testdata.getProperty("validusername"),prop_testdata.getProperty("invalidpassword"));
        myAccountPage=loginPage.accountPage();
        Assert.assertTrue(myAccountPage.accountLoginFailure(),"Warning Message displayed for invaliduserinvalidpassword");
    }

    @Test(priority = 3)
    public void login_invaliduser_validpassword(){
        login_dataentry();
        loginPage.enter_username_password(prop_testdata.getProperty("invalidusername"),prop_testdata.getProperty("validpassword"));
        myAccountPage=loginPage.accountPage();
        Assert.assertTrue(myAccountPage.accountLoginFailure(),"Warning Message displayed for invaliduserinvalidpassword)");
    }

    @Test(priority = 4)
    public void login_invaliduser_invalidpassword(){
        login_dataentry();
        loginPage.enter_username_password(prop_testdata.getProperty("invalidusername"),prop_testdata.getProperty("invalidpassword"));
        myAccountPage=loginPage.accountPage();
        Assert.assertTrue(myAccountPage.accountLoginFailure(),"Warning Message displayed for invaliduserinvalidpassword)");

    }

    @Test(priority = 5)
    public void login_nocredentials(){
        login_dataentry();
        loginPage.enter_username_password("","");
        myAccountPage=loginPage.accountPage();
        Assert.assertTrue(myAccountPage.accountLoginFailure(),"Warning Message displayed for invaliduserinvalidpassword)");

    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }



}
